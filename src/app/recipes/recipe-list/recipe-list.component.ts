import { Component, OnInit } from '@angular/core';

import { Recipe } from '../recipe.model'
import { RecipeService } from '../recipe.service'


@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
    recipeDetail: Recipe;
    
    recipes: Recipe[];
    
//    recipes: Recipe[] = [
//        new Recipe('Test Recipe', 'this is for testing only', 'https://static01.nyt.com/images/2015/08/14/dining/14ROASTEDSALMON/14ROASTEDSALMON-superJumbo.jpg')
//    ];
    
//    @Output() recipeWasSelected = new EventEmitter<Recipe>();
    
    constructor(private recepeService: RecipeService){}

  ngOnInit() {
      this.recipes = this.recepeService.getRecipes();
  }
    

}
