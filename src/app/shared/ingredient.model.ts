export class Ingredient{
    public  name: string;
    public  amount: number;
    
    constructor(name: string, amount: number) {
        this.name = name;
        this.amount = amount;
    }
    // or typescript proveds a shortcut like below
//     constructor(public name: string, public amount: number) {
//        
//    }
}