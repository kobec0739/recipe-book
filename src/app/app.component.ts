import { Component } from '@angular/core';

import { Recipe } from './recipes/recipe.model'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
    recipe: boolean = true;
    shoppingList: boolean = false;
    
    recipeDetail: Recipe;

    switchView(viewData: {recipe: boolean, shoppingList: boolean}) {
        this.recipe = viewData.recipe;
        this.shoppingList = viewData.shoppingList;
    }

 
}