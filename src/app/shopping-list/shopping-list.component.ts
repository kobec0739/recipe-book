import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

import { ShoppingListService } from './shopping-list.service'

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
    ingredients: Ingredient[];
    
//    ingredients: Ingredient[] = [
//        new Ingredient('Apple', 5),
//        new Ingredient('Orange', 10)
//    ];
  constructor(private shoppingListService: ShoppingListService ) { }

  ngOnInit() {
      this.ingredients = this.shoppingListService.getIngredients();
      this.shoppingListService.ingredientsChanged.subscribe(
        (ingredients: Ingredient[]) => {
            this.ingredients = ingredients;
        }
      );
  }
    
    onItemAdded(ingredientData: Ingredient) {
//        this.ingredients.push(ingredientData);
//        this.shoppingListService.(ingredientData);
    }
}
