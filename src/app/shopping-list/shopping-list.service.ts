import { EventEmitter, Injectable } from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';

export class ShoppingListService{
    
    ingredientsChanged = new EventEmitter<Ingredient[]>();
    private ingredients: Ingredient[] = [
        new Ingredient('Apple', 5),
        new Ingredient('Orange', 10)
    ];
    
    getIngredients() {
        // return copy of the array
        return this.ingredients.slice();
    }
    
    addItem(ingredient: Ingredient) {
        this.ingredients.push(ingredient);
        this.ingredientsChanged.emit(this.ingredients.slice());
    }

    addIngredients(ingredients: Ingredient[]) {
        // spread ingredients array and add item one by one
        this.ingredients.push(...ingredients);
        this.ingredientsChanged.emit(this.ingredients.slice());
    }
}