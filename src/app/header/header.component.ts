import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html'
})

export class HeaderComponent {
    @Input() recipeDisplay: boolean = true;
    @Input() shoppingListDisplay: boolean = false;
     
     @Output() switchView = new EventEmitter<{recipe: boolean, shoppingList: boolean}>();
    
    
    displayShoppingList() {
        this.shoppingListDisplay = true;
        this.recipeDisplay = false;
        this.switchView.emit({
           recipe: this.recipeDisplay,
            shoppingList: this.shoppingListDisplay
        });
    }
    
    displayRecipies() {
        this.shoppingListDisplay = false;
        this.recipeDisplay = true;
        this.switchView.emit({
           recipe: this.recipeDisplay,
            shoppingList: this.shoppingListDisplay
        });
    }
}